function removeWelcome() {
    setTimeout(function () {
        removeWelcomeImmediately();
    }, 5000);
}

function removeWelcomeImmediately() {
    var welcomeLayer = document.getElementById('layer-welcome');
    welcomeLayer.style.pointerEvents = 'none';
    welcomeLayer.style.opacity = '0';
}

function turnOn(currentCard) {
    var cardLayer = document.getElementById('layer-card');
    if (!cardLayer.classList.contains('in-front') && !currentCard.classList.contains('on')) {
        currentCard.classList.add('on');
        currentCard.classList.remove('off');
    }
}

function turnOff(currentCard) {
    var cardLayer = document.getElementById('layer-card');
    if (!cardLayer.classList.contains('in-front') && !currentCard.classList.contains('off')) {
        currentCard.classList.remove('on');
        currentCard.classList.add('off');
    }
}

function bringToFront() {
    var cardLayer = document.getElementById('layer-card');
    cardLayer.classList.toggle('in-front');

    var tableSkin = document.getElementById('table-skin');
    tableSkin.classList.toggle('blur');

}